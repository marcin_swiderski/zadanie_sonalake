import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class CalculatorPage {

    private WebDriver webDriver;

    @FindBy(css = "button#Btn3")
    private WebElement buttonNumberThree;

    @FindBy(css = "button#Btn5")
    private WebElement buttonNumberFive;

    @FindBy(css = "button#BtnMult")
    private WebElement buttonMultiply;

    @FindBy(css = "button#Btn9")
    private WebElement buttonNumberNine;

    @FindBy(css = "button#BtnPlus")
    private WebElement buttonPlusSign;

    @FindBy(css = "button#BtnParanL")
    private WebElement buttonLeftBracket;

    @FindBy(css = "button#Btn1")
    private WebElement buttonNumberOne;

    @FindBy(css = "button#Btn0")
    private WebElement buttonNumberZero;

    @FindBy(css = "button#BtnDiv")
    private WebElement buttonDivide;

    @FindBy(css = "button#Btn4")
    private WebElement buttonNumberFour;

    @FindBy(css = "button#BtnParanR")
    private WebElement buttonRightBracket;

    @FindBy(css = "button#BtnCalc")
    private WebElement buttonCalculate;

    @FindBy(css = "button#BtnCos")
    private WebElement buttonCos;

    @FindBy(css = "input#trigorad")
    private WebElement buttonTriggerRadians;

    @FindBy( css = "button#BtnSqrt")
    private WebElement buttonSquareRoot;

    @FindBy( css = "button#Btn8")
    private WebElement buttonNumberEight;

    @FindBy( css = "input#input")
    private WebElement input;

    @FindBy( css = "button#BtnPi")
    private WebElement buttonPi;

    @FindBy( css = "button.fc-cta-consent")
    private WebElement consentButton;

    @FindBy(css = "button.pull-right.dropdown-toggle")
    private WebElement dropdownPreviousEquationsResults;

    @FindBy(css = "div#histframe>ul>li>p.l")
    private List<WebElement> listOfPreviousEquations;

    @FindBy(css = "button#BtnClear")
    private WebElement buttonClear;

    CalculatorPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    public CalculatorPage concentPersonalData(){
        consentButton.click();
        return this;
    }
    public CalculatorPage inputEquation(String equation){
        input.sendKeys(equation);
        String var = input.getAttribute("value");
        assertThat(var).as("Input Equation -  validated").isEqualTo(equation);
        return this;
    }
    public CalculatorPage validateResult(String result){
        String var = input.getAttribute("value");
        assertThat(var).as("Result -  validated").isEqualTo(result);
        return this;
    }
    public CalculatorPage validatePreviousEquations(List<String> listOfEquations){
        List<String> var = new ArrayList<>();
        for (WebElement el: listOfPreviousEquations) {
            var.add(el.getText());
        }
        assertThat(var).as("Validating history").containsOnlyElementsOf(listOfEquations);
        return this;
    }
    public CalculatorPage clickClear(){
        buttonClear.click();
        return this;
    }
    public CalculatorPage clickDropdown(){
        dropdownPreviousEquationsResults.click();
        return this;
    }
    public CalculatorPage clickThree() {
        buttonNumberThree.click();
        return this;
    }
    public CalculatorPage clickPi() {
        buttonPi.click();
        return this;
    }
    public CalculatorPage clickFive() {
        buttonNumberFive.click();
        return this;
    }
    public CalculatorPage clickMultiply() {
        buttonMultiply.click();
        return this;
    }
    public CalculatorPage clickNine() {
        buttonNumberNine.click();
        return this;
    }
    public CalculatorPage clickPlus() {
        buttonPlusSign.click();
        return this;
    }
    public CalculatorPage clickLeftBracket() {
        buttonLeftBracket.click();
        return this;
    }
    public CalculatorPage clickOne() {
        buttonNumberOne.click();
        return this;
    }
    public CalculatorPage clickZero() {
        buttonNumberZero.click();
        return this;
    }
    public CalculatorPage clickDivide() {
        buttonDivide.click();
        return this;
    }
    public CalculatorPage clickFour() {
        buttonNumberFour.click();
        return this;
    }
    public CalculatorPage clickRightBracket() {
        buttonRightBracket.click();
        return this;
    }
    public CalculatorPage clickCalculate() {
        buttonCalculate.click();
        return this;
    }
    public CalculatorPage clickCos() {
        buttonCos.click();
        return this;
    }
    public CalculatorPage clickSquareRoot() {
        buttonSquareRoot.click();
        return this;
    }
    public CalculatorPage clickEight() {
        buttonNumberEight.click();
        return this;
    }
    public CalculatorPage clickRadians() {
        buttonTriggerRadians.click();
        return this;
    }

}
