import io.github.bonigarcia.wdm.WebDriverManager;
import org.assertj.core.api.SoftAssertions;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

import java.util.Arrays;
import java.util.List;


public class BasePageTest {

    private static WebDriver webDriver;

    private static SoftAssertions softAssertions;


    @BeforeAll
    public static void setupClass() {
        WebDriverManager.firefoxdriver().setup();
    }

    @BeforeEach
    void setupTest() {
        webDriver = new FirefoxDriver();
        webDriver.get("https://web2.0calc.com/");
    }

    @AfterMethod
    void makeAsserions() {
        webDriver.quit();
    }
    @AfterTest
    void testTeardown(){
        webDriver.quit();
    }

    @Test
    void oneBigTestToValidateEverythingNeeded(){
        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
        // 1.
        calculatorPage
                .concentPersonalData()
                .clickThree()
                .clickFive()
                .clickMultiply()
                .clickNine()
                .clickNine()
                .clickNine()
                .clickPlus()
                .clickLeftBracket()
                .clickOne()
                .clickZero()
                .clickZero()
                .clickDivide()
                .clickFour()
                .clickRightBracket()
                .clickCalculate()
                .validateResult("34990")
                .clickClear();
        // 2.

        calculatorPage
                .clickRadians()
                .clickCos()
                .clickPi()
                .clickRightBracket()
                .clickCalculate()
                .validateResult("-1")
                .clickClear();

        // 3.
        calculatorPage
                .clickSquareRoot()
                .clickEight()
                .clickOne()
                .clickRightBracket()
                .clickCalculate()
                .validateResult("9")
                .clickClear();

        // 4.
        calculatorPage
                .clickDropdown()
                .validatePreviousEquations(
                        Arrays.asList("35*999+(100/4)","cos(pi)","sqrt(81)"));

    }
    /**
     *  test commented, but functional,and working
     */

//    @Test
//    void calculateGivenEquationByClickingDedicatedButtons(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .clickThree()
//                .clickFive()
//                .clickMultiply()
//                .clickNine()
//                .clickNine()
//                .clickNine()
//                .clickPlus()
//                .clickLeftBracket()
//                .clickOne()
//                .clickZero()
//                .clickZero()
//                .clickDivide()
//                .clickFour()
//                .clickRightBracket()
//                .clickCalculate()
//                .validateResult("34990");
//
//    }
//    @Test()
//    void calculateGivenEquationByTyping(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .inputEquation("35*999+(100/4)")
//                .clickCalculate()
//                .validateResult("34990");
//    }
//
//    @Test
//    void calculateCosPiEquationByClickingDedicatedButtons(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .clickCos()
//                .clickPi()
//                .clickRightBracket()
//                .clickCalculate()
//                .validateResult("-1");
//    }

    /**
     *  test commented only due checkhistory() describtion it the task
     *  it is fully functional
     */
//    @Test
//    void calculateCosPiEquationByTyping(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .clickRadians()
//                .inputEquation("cos(pi)")
//                .clickCalculate()
//                .validateResult("-1");
//    }

//    @Test
//    void calculateSqrtByDedicatedButtons(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .clickSquareRoot()
//                .clickEight()
//                .clickOne()
//                .clickRightBracket()
//                .clickCalculate()
//                .validateResult("9");
//
//    }

    /**
     *  test commented only due checkhistory() describtion it the task
     *  it is fully functional
     */

//    @Test
//    void calculateSqrtByTyping(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .inputEquation("sqrt(81)")
//                .clickCalculate()
//                .validateResult("9");
//
//    }

//    @Test
//    void checkHistory(){
//        CalculatorPage calculatorPage = new CalculatorPage(webDriver);
//
//        calculatorPage
//                .concentPersonalData()
//                .clickDropdown()
//                .validatePreviousEquations(Arrays.asList("35*999+(100/4)","cos(pi)","sqrt(81)"));
//
//    }
}
